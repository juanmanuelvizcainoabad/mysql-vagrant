# README #

This Vagrantfile will create three servers debian jessie and install mysql-server 5.6 and another to use with maxscale

Only use in testing purpose.

### What is this repository for? ###

This Vagrantfile will create three servers debian jessie and install mysql-server 5.6 and another to use with maxscale

### How do I get set up? ###

The password for access to mysql is root and is already configured in my.cnf to access whithout type pass

### Notes ###


```
#!mysql

#####MYSQL

#Insert i an loop
for i in `seq 1 1000`; do mysql -e "insert into test.test (campo1,campo2,campo3,campo4) values (1,1,1,1)" ; done

#server-id
#log-bin = mysql-bin
#Config replication
CHANGE MASTER TO MASTER_HOST='172.17.8.101', MASTER_USER='root', MASTER_PASSWORD='root';

#create user
GRANT ALL PRIVILEGES ON *.* To 'user'@'%' IDENTIFIED BY 'password';
flush privileges;


#### MAXSCALE


CREATE USER 'maxscale'@'172.17.8.100' IDENTIFIED BY 'pass';
GRANT SELECT ON mysql.user TO 'maxscale'@'172.17.8.100';
GRANT SELECT ON mysql.db TO 'maxscale'@'172.17.8.100';
GRANT SELECT ON mysql.tables_priv TO 'maxscale'@'172.17.8.100';
GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'maxscale'@'172.17.8.100' IDENTIFIED BY 'pass';
flush privileges;


```