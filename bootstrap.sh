#!/bin/bash
password="root"


echo "Setting variables"
echo 'mysql-community-server mysql-server/root_password password' $password  | debconf-set-selections
echo 'mysql-community-server mysql-server/root_password_again password' $password | debconf-set-selections
echo 'mysql-apt-config mysql-apt-config/enable-repo select mysql-5.6' | debconf-set-selections
echo 'mysql-community-server mysql-community-server/root-pass password' $password | debconf-set-selections
echo 'mysql-community-server mysql-community-server/re-root-pass password' $password | debconf-set-selections
echo 'mysql-community-server mysql-community-server/data-dir note ok' | debconf-set-selections
echo "Finish setting variables"

echo "instaling mysql server"
wget http://repo.mysql.com/mysql-apt-config_0.2.1-1debian7_all.deb
dpkg -i mysql-apt-config_0.2.1-1debian7_all.deb


apt-get update
export DEBIAN_FRONTEND=noninteractive
apt-get install mysql-server-5.6 -y
echo "Finish mysql intallation"
echo "configuring mysql-server"
sed -i  's/.*client.*/&\npassword = root/' /etc/mysql/my.cnf
sed -i '/bind-address/g' /etc/mysql/my.cnf
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '"$password"';flush privileges;"
systemctl  restart mysql.service
